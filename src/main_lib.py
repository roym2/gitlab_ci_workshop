import sys
import os


def make_str_lower(s):
    return s.lower()


def check_py_version():
    python_major_version = sys.version_info[0]
    python_minor_version = sys.version_info[1]
    if python_major_version != 3 or python_minor_version != 8:
        print("Python version must be 3.8.*")
        sys.exit(1)


def check_connection_to_postgres():
    import socket                                                                                                                                                              
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect(('postgres',5432))
        s.close()
    except socket.error as ex:
        print("Postgres connection failed {0}: {1}".format(ex.errno, ex.strerror))            
        sys.exit(1)


if __name__ == "__main__":
    check_py_version()
    check_connection_to_postgres()
