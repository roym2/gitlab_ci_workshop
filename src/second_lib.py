import sys
import os

def check_if_file_exists(file_path):
    if not os.path.isfile(file_path):
        print("File {0} not found".format(file_path))
        sys.exit(1)


if __name__ == "__main__":
    check_if_file_exists("dist/build.txt")
